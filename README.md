# OpenML dataset: Stylized_Meta_Album_DOG_STY_Extended

https://www.openml.org/d/46037

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Dogs dataset with different breeds of dogs (stylized)

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46037) of an [OpenML dataset](https://www.openml.org/d/46037). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46037/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46037/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46037/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

